'use strict';

var simulationACA = new ACA({
	domElement: DOM.el.id('simulation'),
	architecture: CellArchitecture.VonNeumanToroidal,
	architectureRules: {
		min: { x: -25, y: -25 },
		firstCells: { x: 10, y: 10 },
		max: { x: 25, y: 25 }
	},
	geometry: CellGeometry.VonNeumanSquare,
	updateFrequencyMS: 10,
	updatesPerUpdate: 20,
	cameraPosition: { x: 30, y: 50, z: 30 }
});

var dragPos = {};
var cameraRotation = { horizontal: 0, vertical: 0 };

DOM.modify.addListeners(DOM.el.id('simulation'), {
	dragstart: function dragstart(event) {

		dragPos.x = event.clientX;
		dragPos.y = event.clientY;
	},
	drag: function drag(event) {

		if (!event.clientX && !event.clientY) return;

		var dragChange = { x: event.clientX - dragPos.x, y: event.clientY - dragPos.y };

		dragPos.x = event.clientX;
		dragPos.y = event.clientY;

		cameraRotation.horizontal += dragChange.x / 150;
		cameraRotation.vertical -= dragChange.y / 150;

		if (cameraRotation.vertical < .1) cameraRotation.vertical = .1;else if (cameraRotation.vertical > Math.PI / 2 - .1) cameraRotation.vertical = Math.PI / 2 - .1;

		simulationACA.setCameraPosition({
			x: 30 * Math.sin(cameraRotation.horizontal) * Math.cos(cameraRotation.vertical),
			y: 30 * Math.sin(cameraRotation.vertical),
			z: 30 * Math.cos(cameraRotation.horizontal) * Math.cos(cameraRotation.vertical)
		});
	}
});

var playpause = DOM.el.id('simulation-controls-playpause');

DOM.modify.addListeners(playpause, {
	click: function click() {

		if (simulationACA.isRunning()) {

			simulationACA.stopRunning();
			playpause.textContent = 'Run simulation';
		} else {

			simulationACA.startRunning();
			playpause.textContent = 'Pause simulation';
		}
	}
});

DOM.modify.addListeners(DOM.el.id('simulation-controls-step'), {
	click: function click() {

		simulationACA.stopRunning();
		playpause.textContent = 'Run simulation';

		simulationACA.updateCell();
		simulationACA.redraw();
	}
});

DOM.modify.addListeners(DOM.el.id('simulation-controls-reset'), {
	click: function click() {
		return simulationACA.setAllCells(0);
	}
});

var rulesACA = new ACA({
	domElement: DOM.el.id('rules'),
	architecture: CellArchitecture.VonNeumanBasic,
	architectureRules: {
		firstCells: { x: 0, y: 0 },
		iterations: 2
	},
	geometry: CellGeometry.VonNeumanSquare,
	updateFrequencyMS: Infinity,
	cameraPosition: { x: 0, y: 3, z: 0 }
});

var resultACA = new ACA({
	domElement: DOM.el.id('result'),
	architecture: CellArchitecture.VonNeumanBasic,
	architectureRules: {
		firstCells: { x: 0, y: 0 },
		iterations: 1
	},
	geometry: CellGeometry.VonNeumanSquare,
	updateFrequencyMS: Infinity,
	cameraPosition: { x: 0, y: 3, z: 0 }
});

var symmetrical = DOM.el.id('rule-controls-symmetrical');
var reflective = DOM.el.id('rule-controls-reflective');

var ruleDisplay = DOM.el.id('rule-display');

DOM.modify.addListeners(DOM.el.id('rule-controls-saveconfig'), {
	click: function click(event) {

		var center = rulesACA.getCell(0);
		var result = resultACA.getCell(0);

		simulationACA.addRule({
			center: center.state,
			up: rulesACA.getCellKey(center.neighbourKeys.up).state,
			right: rulesACA.getCellKey(center.neighbourKeys.right).state,
			down: rulesACA.getCellKey(center.neighbourKeys.down).state,
			left: rulesACA.getCellKey(center.neighbourKeys.left).state,
			result: result.state,
			symmetric: symmetrical.checked,
			reflective: reflective.checked
		});
	}
});

simulationACA.addListener('addedRule', function (_ref) {
	var key = _ref.key;
	var value = _ref.value;

	var keys = key.split('~');

	var center = +keys[0];
	var up = +keys[1];
	var right = +keys[2];
	var down = +keys[3];
	var left = +keys[4];
	var result = value;

	var centerCell = rulesACA.getCell(0);
	var upCell = rulesACA.getCellKey(centerCell.neighbourKeys.up);
	var rightCell = rulesACA.getCellKey(centerCell.neighbourKeys.right);
	var downCell = rulesACA.getCellKey(centerCell.neighbourKeys.down);
	var leftCell = rulesACA.getCellKey(centerCell.neighbourKeys.left);
	var resultCell = resultACA.getCell(0);

	var centerState = centerCell.state;
	var upState = upCell.state;
	var rightState = rightCell.state;
	var downState = downCell.state;
	var leftState = leftCell.state;
	var resultState = resultCell.state;

	centerCell.set(center);
	upCell.set(up);
	rightCell.set(right);
	downCell.set(down);
	leftCell.set(left);
	resultCell.set(result);

	rulesACA.forceRedraw();
	resultACA.forceRedraw();

	DOM.modify.appendNode(ruleDisplay, DOM.gen.element({
		type: 'div',
		styles: {
			display: 'inline-block'
		},
		nodes: [rulesACA.dumpToImage(), resultACA.dumpToImage()],
		data: {
			key: key
		},
		listeners: {
			click: function click() {

				simulationACA.removeRule({ center: center, up: up, right: right, down: down, left: left });
			}
		}
	}));

	centerCell.set(centerState);
	upCell.set(upState);
	rightCell.set(rightState);
	downCell.set(downState);
	leftCell.set(leftState);
	resultCell.set(resultState);

	rulesACA.forceRedraw();
	resultACA.forceRedraw();
});

simulationACA.addListener('removedRule', function (_ref2) {
	var key = _ref2.key;

	DOM.el.queryFirst(ruleDisplay, 'div[data-key="' + key + '"]').remove();
});

DOM.modify.addListeners(DOM.el.id('loadsave-controls-saverules'), {
	click: function click() {

		Utils.SaveFile({
			title: Date.now() + '.acarules',
			text: simulationACA.getRulesCompressed()
		});
	}
});

DOM.modify.addListeners(DOM.el.id('loadsave-controls-loadrules'), {
	click: function click() {
		return Utils.LoadFile(function (contents) {
			return simulationACA.loadRulesCompressed(contents);
		});
	}
});

DOM.modify.addListeners(DOM.el.id('loadsave-controls-clearrules'), {
	click: function click() {
		return simulationACA.clearRules();
	}
});