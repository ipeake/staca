'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var OMap = (function (_Observable) {
	_inherits(OMap, _Observable);

	function OMap() {
		_classCallCheck(this, OMap);

		_get(Object.getPrototypeOf(OMap.prototype), 'constructor', this).call(this);

		this._o = {};
	}

	_createClass(OMap, [{
		key: 'get',
		value: function get(key) {

			return this._o[key];
		}
	}, {
		key: 'set',
		value: function set(key, value) {

			this['delete'](key);

			this._o[key] = value;

			this._triggerEvent('addedData', { key: key, value: value });
		}
	}, {
		key: 'has',
		value: function has(key) {

			return !T.Undefined(this.get(key));
		}
	}, {
		key: 'size',
		value: function size() {

			return this.keys().length;
		}
	}, {
		key: 'delete',
		value: function _delete(key) {

			if (!T.Undefined(this._o[key])) {

				var value = this._o[key];

				delete this._o[key];

				this._triggerEvent('removedData', { key: key, value: value });
			}
		}
	}, {
		key: 'forEach',
		value: function forEach(f) {

			Functional.eachKey(this._o, f);
		}
	}, {
		key: 'key',
		value: function key(i) {

			return Functional.keys(this._o)[i];
		}
	}, {
		key: 'keys',
		value: function keys() {

			return Functional.keys(this._o);
		}
	}, {
		key: 'entry',
		value: function entry(i) {

			return this._o[this.key(i)];
		}
	}, {
		key: 'entries',
		value: function entries() {

			return Functional.keyValuePairs(this._o);
		}
	}, {
		key: 'empty',
		value: function empty() {
			var _this = this;

			this.forEach(function (_ref) {
				var key = _ref.key;
				return _this['delete'](key);
			});
		}
	}, {
		key: 'toJSONString',
		value: function toJSONString() {
			return JSON.stringify(this._o);
		}
	}, {
		key: 'fromJSONString',
		value: function fromJSONString(jsons) {
			var _this2 = this;

			this.empty();

			this._o = JSON.parse(jsons);
			this.forEach(function (_ref2) {
				var key = _ref2.key;
				var value = _ref2.value;
				return _this2._triggerEvent('addedData', { key: key, value: value });
			});
		}
	}]);

	return OMap;
})(Observable);

;