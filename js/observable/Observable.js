'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var Observable = (function () {
	function Observable() {
		_classCallCheck(this, Observable);

		this._listeners = {};
	}

	_createClass(Observable, [{
		key: '_triggerEvent',
		value: function _triggerEvent(type, data) {

			if (data) {

				data.type = type;
			} else {

				data = { type: type };
			}

			if (this._listeners[type]) {

				this._listeners[type].forEach(function (l) {
					return l(data);
				});
			}

			if (type !== '*' && this._listeners['*']) {

				this._listeners['*'].forEach(function (l) {
					return l(data);
				});
			}
		}
	}, {
		key: 'addListener',
		value: function addListener(type, listener) {

			(this._listeners[type] || (this._listeners[type] = new Set())).add(listener);
		}
	}, {
		key: 'removeListener',
		value: function removeListener(type, listener) {

			if (this._listeners[type]) this._listeners[type]['delete'](listener);
		}
	}]);

	return Observable;
})();

;