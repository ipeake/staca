'use strict';

var Utils = {
	NextTick: function NextTick(func) {
		return setTimeout(func, 0);
	},
	ToroidalNum: function ToroidalNum(num, min, max) {
		return num == min - 1 ? max : num == max + 1 ? min : num;
	},
	SaveFile: (function () {

		var a = document.createElement('a');

		return function (_ref) {
			var title = _ref.title;
			var text = _ref.text;
			var _ref$type = _ref.type;
			var type = _ref$type === undefined ? 'text/plain' : _ref$type;

			a.href = URL.createObjectURL(new Blob([text], { type: type }));
			a.download = title;

			a.click();
		};
	})(),
	LoadFile: (function () {

		var i = DOM.gen.element({
			type: 'input',
			styles: {
				display: 'none'
			},
			attributes: {
				type: 'file'
			}
		});

		DOM.modify.addToBody(i);

		return function (callback) {

			i.onchange = function (event) {

				Array.prototype.forEach.call(event.target.files, function (file) {

					var reader = new FileReader();
					reader.onload = function (e) {
						return callback(e.target.result);
					};
					reader.readAsText(file);
				});
			};
			i.click();
		};
	})()
};