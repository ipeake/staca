'use strict';

var DOM = {
	el: {
		'class': function _class(par, clas) {
			return par.getElementsByClassName(clas);
		},
		id: function id(_id) {
			return document.getElementById(_id);
		},
		type: function type(par, tagname) {
			return par.getElementsByTagName(tagname);
		},
		query: function query(par, _query) {
			return par.querySelectorAll(_query);
		},
		queryFirst: function queryFirst(par, query) {
			return par.querySelector(query);
		}
	},
	prop: {
		nodes: function nodes(el) {
			return el.childNodes;
		},
		firstNode: function firstNode(el) {
			return el.firstChild;
		},
		nthNode: function nthNode(el, i) {
			return el.childNodes[i];
		},
		lastNode: function lastNode(el) {
			return el.lastChild;
		},
		elements: function elements(el) {
			return el.children;
		},
		firstElement: function firstElement(el) {
			return el.firstElementChild;
		},
		nthElement: function nthElement(el, i) {
			return el.children[i];
		},
		lastElement: function lastElement(el) {
			return el.lastElementChild;
		},
		width: function width(el) {
			return el.clientWidth;
		},
		height: function height(el) {
			return el.clientHeight;
		},
		left: function left(el) {
			return el.offsetLeft;
		},
		top: function top(el) {
			return el.offsetTop;
		}
	},
	modify: {
		prependNode: function prependNode(el, child) {

			el.insertBefore(child, DOM.prop.firstNode(el));

			return el;
		},
		insertNode: function insertNode(el, i) {

			el.insertBefore(child, DOM.prop.nthNode(el, i));

			return el;
		},
		appendNode: function appendNode(el, child) {

			el.appendChild(child);

			return el;
		},
		prependNodes: function prependNodes(el, children) {

			Functional.each(Functional.reverse(children), Functional.curry(DOM.modify.prependNode, el));
		},
		insertNodes: function insertNodes(el, children, i) {

			Functional.each(Functional.reverse(children), Functional.curry(DOM.modify.insertNode, Functional._, i));

			return el;
		},
		appendNodes: function appendNodes(el, children) {

			Functional.each(children, Functional.curry(DOM.modify.appendNode, el));

			return el;
		},
		addClass: function addClass(el, clas) {

			el.classList.add(clas);

			return el;
		},
		addClasses: function addClasses(el, classes) {

			Functional.each(classes, Functional.curry(DOM.modify.addClass(el)));

			return el;
		},
		addStyles: function addStyles(el, styles) {

			Functional.eachKey(styles, function (_ref) {
				var key = _ref.key;
				var value = _ref.value;
				return el.style[key] = value;
			});

			return el;
		},
		addListeners: function addListeners(el, listeners) {

			Functional.eachKey(listeners, function (_ref2) {
				var key = _ref2.key;
				var value = _ref2.value;
				return el.addEventListener(key, value);
			});

			return el;
		},
		addToBody: function addToBody() {

			var queue = [];

			document.addEventListener('DOMContentLoaded', function () {

				DOM.modify.appendNodes(document.body, queue);

				queue = null;
				DOM.modify.addToBody = Functional.curry(DOM.modify.appendNode, document.body);
			});

			return function (node) {

				queue.push(node);

				return node;
			};
		}
	},
	gen: {
		element: function element(_ref3) {
			var type = _ref3.type;
			var id = _ref3.id;
			var classes = _ref3.classes;
			var nodes = _ref3.nodes;
			var styles = _ref3.styles;
			var listeners = _ref3.listeners;
			var attributes = _ref3.attributes;
			var data = _ref3.data;

			var newE = document.createElement(type);

			if (classes) DOM.modify.addClasses(newE, classes);
			if (id) newE.id = id;
			if (styles) DOM.modify.addStyles(newE, styles);
			if (listeners) DOM.modify.addListeners(newE, listeners);
			if (nodes) DOM.modify.appendNodes(newE, nodes);
			if (attributes) Functional.eachKey(attributes, function (_ref4) {
				var key = _ref4.key;
				var value = _ref4.value;
				return newE[key] = value;
			});
			if (data) Functional.eachKey(data, function (_ref5) {
				var key = _ref5.key;
				var value = _ref5.value;
				return newE.dataset[key] = value;
			});

			return newE;
		},
		text: function text(content) {
			return document.createTextNode(content);
		}
	}
};