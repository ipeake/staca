"use strict";

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

var Functional = {
	curry: (function () {

		var _ = Symbol();

		var curry = function curry(func) {
			for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
				args[_key - 1] = arguments[_key];
			}

			var innerFunc = function innerFunc() {
				for (var _len2 = arguments.length, innerArgs = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
					innerArgs[_key2] = arguments[_key2];
				}

				var innerI = 0;
				var theseArgs = args.slice();

				for (var i = 0; i < theseArgs.length && innerI < innerArgs.length; ++i) {

					if (theseArgs[i] === _) {

						theseArgs[i] = innerArgs[innerI];

						++innerI;
					}
				}

				theseArgs = theseArgs.concat(innerArgs.slice(innerI));

				if (theseArgs.length >= func.length && theseArgs.every(function (x) {
					return x !== _;
				})) {

					return func.apply(undefined, _toConsumableArray(theseArgs));
				} else {

					args = theseArgs;

					return innerFunc;
				}
			};

			return innerFunc;
		};

		curry._ = _;

		return curry;
	})(),
	// Properties
	first: function first(xs) {
		return xs[0];
	},
	nth: function nth(xs, n) {
		return n >= 0 ? xs[n] : xs[length + n];
	},
	last: function last(xs) {
		return xs[xs.length - 1];
	},
	keys: function keys(obj) {
		return Object.keys(obj);
	},
	keyValuePairs: function keyValuePairs(obj) {
		return Functional.map(Functional.keys(obj), function (key) {
			return { key: key, value: obj[key] };
		});
	},
	reverse: function reverse(arrayLike) {
		return Functional.cloneShallow(arrayLike).reverse();
	},
	// Iteration
	each: function each(arrayLike, func) {

		Array.prototype.forEach.call(arrayLike, func);

		return arrayLike;
	},
	eachKey: function eachKey(obj, func) {
		return Functional.each(Functional.keyValuePairs(obj), func);
	},
	any: function any(arrayLike, predicate) {
		return Array.prototype.some.call(arrayLike, predicate);
	},
	anyWithRes: function anyWithRes(arrayLike, predicate) {

		var res = undefined;

		Functional.any(arrayLike, function (x) {
			return res = predicate(x);
		});

		return res || void 0;
	},
	all: function all(arrayLike, predicate) {
		return Array.prototype.every.call(arrayLike, predicate);
	},
	fold: function fold(arrayLike, func, initialVal) {
		return Array.prototype.reduce.call(arrayLike, func, initialVal);
	},
	map: function map(arrayLike, func) {
		return Array.prototype.map.call(arrayLike, func);
	},
	cloneShallow: function cloneShallow(arrayLike) {
		return Array.prototype.slice.call(arrayLike);
	}
};