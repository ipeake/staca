'use strict';

var T = {
	Null: function Null(x) {
		return x === null && T.Null;
	},
	Undefined: function Undefined(x) {
		return x === undefined && T.Undefined;
	},
	Number: (function (_Number) {
		function Number(_x) {
			return _Number.apply(this, arguments);
		}

		Number.toString = function () {
			return _Number.toString();
		};

		return Number;
	})(function (x) {
		return x.__proto__ === Number && T.Number;
	}),
	Integer: function Integer(x) {
		return T.Number(x) && x === (x | 0) && T.Integer;
	},
	Float: function Float(x) {
		return T.Number(x) && !T.Integer(x) && T.Float;
	},
	String: (function (_String) {
		function String(_x2) {
			return _String.apply(this, arguments);
		}

		String.toString = function () {
			return _String.toString();
		};

		return String;
	})(function (x) {
		return x.__proto__ === String && T.String;
	}),
	is: function is(type, x) {
		return T.Undefined(x) || type(x) || T.is(T.__proto__, x);
	},
	first: function first(types, x) {
		return T.Undefined(x) || Functional.anyWithRes(types, type) || T.first(types, x.__proto__);
	}
};