'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x4, _x5, _x6) { var _again = true; _function: while (_again) { var object = _x4, property = _x5, receiver = _x6; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x4 = parent; _x5 = property; _x6 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var States = {
	0: 0,
	1: 1,
	2: 2,
	3: 3,
	4: 4,
	5: 5,
	6: 6,
	7: 7,
	length: 8,
	previous: {
		0: 7,
		1: 0,
		2: 1,
		3: 2,
		4: 3,
		5: 4,
		6: 5,
		7: 6
	},
	next: {
		0: 1,
		1: 2,
		2: 3,
		3: 4,
		4: 5,
		5: 6,
		6: 7,
		7: 0
	}
};

var Colors = {
	0: 0xffffff,
	1: 0xff0000,
	2: 0x00ff00,
	3: 0x0000ff,
	4: 0xffff00,
	5: 0xff00ff,
	6: 0x00ffff,
	7: 0xff9999
};

var ACA = (function (_Observable) {
	_inherits(ACA, _Observable);

	function ACA(_ref) {
		var domElement = _ref.domElement;
		var architecture = _ref.architecture;
		var architectureRules = _ref.architectureRules;
		var geometry = _ref.geometry;
		var _ref$updateFrequencyMS = _ref.updateFrequencyMS;
		var updateFrequencyMS = _ref$updateFrequencyMS === undefined ? 100 : _ref$updateFrequencyMS;
		var _ref$updatesPerUpdate = _ref.updatesPerUpdate;
		var updatesPerUpdate = _ref$updatesPerUpdate === undefined ? 10 : _ref$updatesPerUpdate;
		var _ref$camera = _ref.camera;
		var camera = _ref$camera === undefined ? {
			fov: 70,
			aspect: DOM.prop.width(domElement) / DOM.prop.height(domElement),
			near: 1,
			far: 100
		} : _ref$camera;
		var _ref$cameraPosition = _ref.cameraPosition;
		var cameraPosition = _ref$cameraPosition === undefined ? { x: 0, y: 0, z: 0 } : _ref$cameraPosition;
		return (function () {
			var _this = this;

			_classCallCheck(this, ACA);

			_get(Object.getPrototypeOf(ACA.prototype), 'constructor', this).call(this);

			this._threeScene = new THREE.Scene();
			this._threeCamera = new THREE.PerspectiveCamera(camera.fov, camera.aspect, camera.near, camera.far);
			this._threeCamera.position.set(cameraPosition.x, cameraPosition.y, cameraPosition.z);
			this._threeCamera.lookAt(this._threeScene.position);
			this._threeRenderer = new THREE.WebGLRenderer();
			this._threeRenderer.setSize(DOM.prop.width(domElement), DOM.prop.height(domElement));

			this._threeEffect = new THREE.AnaglyphEffect(this._threeRenderer);
			this._threeEffect.setSize(DOM.prop.width(domElement), DOM.prop.height(domElement));

			this._cells = new OMap();
			this._cells.addListener('addedData', function () {

				_this.redraw();
			});

			this._rules = new OMap();
			this._rules.addListener('*', function (data) {

				switch (data.type) {
					case 'addedData':
						return _this._triggerEvent('addedRule', data);
					case 'removedData':
						return _this._triggerEvent('removedRule', data);
				}
			});

			this._simulationRunning = false;
			this._interval = null;
			this._updateFrequencyMS = updateFrequencyMS;
			this._updatesPerUpdate = updatesPerUpdate;
			this._cellArchitecture = architecture;
			this._cellArchitecture.Generate(geometry, this._cells, this._threeScene, architectureRules);

			DOM.modify.addListeners(this._threeRenderer.domElement, {
				click: function click(event) {

					var cell = _this.getCellAtPosition(event.clientX, event.clientY);

					if (cell) {

						cell.leftClick();

						_this.redraw();
					}
				},
				contextmenu: function contextmenu(event) {

					var cell = _this.getCellAtPosition(event.clientX, event.clientY);

					if (cell) {

						cell.rightClick();

						_this.redraw();
					}

					event.preventDefault();
				}
			});

			DOM.modify.appendNode(domElement, this._threeRenderer.domElement);
		}).apply(this, arguments);
	}

	_createClass(ACA, [{
		key: 'isRunning',
		value: function isRunning() {

			return this._simulationRunning;
		}
	}, {
		key: 'startRunning',
		value: function startRunning() {
			var _this2 = this;

			if (this._simulationRunning) return;

			this._simulationRunning = true;
			this._interval = setInterval(function () {

				for (var i = 0; i < _this2._updatesPerUpdate; ++i) {
					_this2.updateCell();
				}_this2.redraw();
			}, this._updateFrequencyMS);
		}
	}, {
		key: 'stopRunning',
		value: function stopRunning() {

			if (!this._simulationRunning) return;

			this._simulationRunning = false;
			clearInterval(this._interval);
		}
	}, {
		key: 'updateCell',
		value: function updateCell() {

			this.getRandomCell().update(this._cells, this._rules);
		}
	}, {
		key: 'setAllCells',
		value: function setAllCells(state) {

			this._cells.forEach(function (_ref2) {
				var value = _ref2.value;
				return value.set(state);
			});

			this.redraw();
		}
	}, {
		key: 'getRandomCell',
		value: function getRandomCell() {

			return this.getCell(Math.floor(this._cells.size() * Math.random()));
		}
	}, {
		key: 'getCell',
		value: function getCell(i) {

			return this._cells.entry(i);
		}
	}, {
		key: 'getCellKey',
		value: function getCellKey(k) {

			return this._cells.get(k);
		}
	}, {
		key: 'getCellAtPosition',
		value: function getCellAtPosition(x, y) {
			var _threeRenderer$domElement$getBoundingClientRect = this._threeRenderer.domElement.getBoundingClientRect();

			var left = _threeRenderer$domElement$getBoundingClientRect.left;
			var top = _threeRenderer$domElement$getBoundingClientRect.top;
			var width = _threeRenderer$domElement$getBoundingClientRect.width;
			var height = _threeRenderer$domElement$getBoundingClientRect.height;

			// Make a vector from -1 to 1 where -1 is left/top and 1 is right/bottom
			var vector = new THREE.Vector3(2 * (x - left) / width - 1, -2 * (y - top) / height + 1, 0.5);

			vector.unproject(this._threeCamera);

			var intersected = new THREE.Raycaster(this._threeCamera.position, vector.sub(this._threeCamera.position).normalize()).intersectObjects(this._threeScene.children);

			if (intersected.length) {

				var first = intersected[0];

				if (first.object && first.object.architecture) {

					return first.object.architecture;
				}
			}
		}
	}, {
		key: 'addRule',
		value: function addRule(acaRule) {

			this._cellArchitecture.AddRule(this._rules, acaRule);
		}
	}, {
		key: 'removeRule',
		value: function removeRule(acaRule) {

			this._cellArchitecture.RemoveRule(this._rules, acaRule);
		}
	}, {
		key: 'getRulesCompressed',
		value: function getRulesCompressed() {

			return this._rules.toJSONString();
		}
	}, {
		key: 'loadRulesCompressed',
		value: function loadRulesCompressed(jsonstring) {

			this.clearRules();

			this._rules.fromJSONString(jsonstring);
		}
	}, {
		key: 'clearRules',
		value: function clearRules() {

			this._rules.empty();
		}
	}, {
		key: 'adjustCameraPosition',
		value: function adjustCameraPosition(_ref3) {
			var x = _ref3.x;
			var y = _ref3.y;
			var z = _ref3.z;

			this.setCameraPosition({
				x: this._threeCamera.position.x + x,
				y: this._threeCamera.position.y + y,
				z: this._threeCamera.position.z + z
			});
		}
	}, {
		key: 'setCameraPosition',
		value: function setCameraPosition(_ref4) {
			var x = _ref4.x;
			var y = _ref4.y;
			var z = _ref4.z;

			this._threeCamera.position.set(x, y, z);
			this._threeCamera.lookAt(this._threeScene.position);

			this.redraw();
		}
	}, {
		key: 'redraw',
		value: function redraw() {
			var _this3 = this;

			var ms = 50;
			var timeout = function timeout() {

				if (!_this3.drawAgain) {

					_this3.drawnRecently = false;

					return;
				}

				_this3.forceRedraw();

				setTimeout(timeout, ms);
			};

			if (this.drawnRecently) {

				this.drawAgain = true;
			} else {

				this.forceRedraw();

				this.drawnRecently = true;

				setTimeout(timeout, ms);
			}
		}
	}, {
		key: 'forceRedraw',
		value: function forceRedraw() {

			//this._threeRenderer.render(this._threeScene, this._threeCamera);
			this._threeEffect.render(this._threeScene, this._threeCamera);
		}
	}, {
		key: 'dumpToImage',
		value: function dumpToImage() {

			// Need to redraw so frame buffer is full
			this.redraw();

			return DOM.gen.element({
				type: 'img',
				styles: {
					width: DOM.prop.width(this._threeRenderer.domElement),
					height: DOM.prop.height(this._threeRenderer.domElement)
				},
				attributes: {
					src: this._threeRenderer.domElement.toDataURL()
				}
			});
		}
	}]);

	return ACA;
})(Observable);

;

var CellArchitecture = (function () {
	var GenericArchitecture = (function () {
		function GenericArchitecture(_ref5) {
			var state = _ref5.state;

			_classCallCheck(this, GenericArchitecture);

			this.state = state;
		}

		_createClass(GenericArchitecture, [{
			key: 'update',
			value: function update(cells, rules) {

				var state = rules.get(this.getStateAsString(cells));

				if (T.Undefined(state) || state == this.state) return;

				this.set(state);
			}
		}, {
			key: 'set',
			value: function set(state) {

				this.state = state;

				this.cellGeometry.update({ state: state });
			}
		}, {
			key: 'leftClick',
			value: function leftClick() {

				this.state = States.next[this.state];
				this.cellGeometry.update({ state: this.state });
			}
		}, {
			key: 'rightClick',
			value: function rightClick() {

				this.state = States.previous[this.state];
				this.cellGeometry.update({ state: this.state });
			}
		}]);

		return GenericArchitecture;
	})();

	;

	var VonNeumanBasic = (function (_GenericArchitecture) {
		_inherits(VonNeumanBasic, _GenericArchitecture);

		function VonNeumanBasic(params) {
			_classCallCheck(this, VonNeumanBasic);

			_get(Object.getPrototypeOf(VonNeumanBasic.prototype), 'constructor', this).call(this, params);

			var x = params.x;
			var y = params.y;

			this.neighbourKeys = {
				up: void 0,
				right: void 0,
				down: void 0,
				left: void 0
			};
			this.x = x;
			this.y = y;
		}

		_createClass(VonNeumanBasic, [{
			key: 'initGeom',
			value: function initGeom(_ref6) {
				var cellGeometryClass = _ref6.cellGeometryClass;
				var architectureRules = _ref6.architectureRules;
				var scene = _ref6.scene;

				this.cellGeometry = new cellGeometryClass({
					x: this.x,
					y: this.y,
					state: this.state,
					architectureRules: architectureRules,
					architecture: this,
					scene: scene
				});

				this.cellGeometry.update({ state: this.state });
			}
		}, {
			key: 'getStateAsString',
			value: function getStateAsString(cells) {

				return this.state + '~' + cells.get(this.neighbourKeys.up).state + '~' + cells.get(this.neighbourKeys.right).state + '~' + cells.get(this.neighbourKeys.down).state + '~' + cells.get(this.neighbourKeys.left).state;
			}
		}], [{
			key: 'AddRule',
			value: function AddRule(rules, _ref7) {
				var center = _ref7.center;
				var up = _ref7.up;
				var right = _ref7.right;
				var down = _ref7.down;
				var left = _ref7.left;
				var result = _ref7.result;
				var _ref7$symmetric = _ref7.symmetric;
				var symmetric = _ref7$symmetric === undefined ? true : _ref7$symmetric;
				var _ref7$reflective = _ref7.reflective;
				var reflective = _ref7$reflective === undefined ? true : _ref7$reflective;

				var add = function add(center, up, right, down, left) {

					rules.set(center + '~' + up + '~' + right + '~' + down + '~' + left, result);

					if (reflective) {

						rules.set(center + '~' + up + '~' + left + '~' + down + '~' + right, result);
					}
				};

				add(center, up, right, down, left);

				if (symmetric) {

					add(center, left, up, right, down);
					add(center, down, left, up, right);
					add(center, right, down, left, up);
				}
			}
		}, {
			key: 'RemoveRule',
			value: function RemoveRule(rules, _ref8) {
				var center = _ref8.center;
				var up = _ref8.up;
				var right = _ref8.right;
				var down = _ref8.down;
				var left = _ref8.left;
				var result = _ref8.result;

				rules['delete'](center + '~' + up + '~' + right + '~' + down + '~' + left);
			}
		}, {
			key: 'Generate',
			value: function Generate(cellGeometryClass, cells, scene, architectureRules) {
				var _ref9 = arguments.length <= 4 || arguments[4] === undefined ? architectureRules.firstCells : arguments[4];

				var x = _ref9.x;
				var y = _ref9.y;
				var remainingIterations = arguments.length <= 5 || arguments[5] === undefined ? architectureRules.iterations : arguments[5];
				return (function () {

					var cellKey = x + '~' + y;

					if (!cells.has(cellKey)) {
						(function () {

							var thisCell = new CellArchitecture.VonNeumanBasic({
								x: x,
								y: y,
								state: States[0]
							});
							thisCell.initGeom({
								cellGeometryClass: cellGeometryClass,
								architectureRules: architectureRules,
								scene: scene
							});

							cells.set(cellKey, thisCell);

							if (remainingIterations - 1 > 0) {

								// Create the neighbours in a queue so that the stack won't overflow
								Utils.NextTick(function () {

									thisCell.neighbourKeys = {
										up: CellArchitecture.VonNeumanBasic.Generate(cellGeometryClass, cells, scene, architectureRules, { x: x, y: y + 1 }, remainingIterations - 1),
										right: CellArchitecture.VonNeumanBasic.Generate(cellGeometryClass, cells, scene, architectureRules, { x: x - 1, y: y }, remainingIterations - 1),
										down: CellArchitecture.VonNeumanBasic.Generate(cellGeometryClass, cells, scene, architectureRules, { x: x, y: y - 1 }, remainingIterations - 1),
										left: CellArchitecture.VonNeumanBasic.Generate(cellGeometryClass, cells, scene, architectureRules, { x: x + 1, y: y }, remainingIterations - 1)
									};
								});
							}
						})();
					}

					return cellKey;
				})();
			}
		}]);

		return VonNeumanBasic;
	})(GenericArchitecture);

	;

	var VonNeumanToroidal = (function (_VonNeumanBasic) {
		_inherits(VonNeumanToroidal, _VonNeumanBasic);

		function VonNeumanToroidal(params) {
			_classCallCheck(this, VonNeumanToroidal);

			_get(Object.getPrototypeOf(VonNeumanToroidal.prototype), 'constructor', this).call(this, params);
		}

		_createClass(VonNeumanToroidal, null, [{
			key: 'Generate',
			value: function Generate(cellGeometryClass, cells, scene, architectureRules) {
				var _ref10 = arguments.length <= 4 || arguments[4] === undefined ? architectureRules.firstCells : arguments[4];

				var x = _ref10.x;
				var y = _ref10.y;
				return (function () {

					x = Utils.ToroidalNum(x, architectureRules.min.x, architectureRules.max.x);
					y = Utils.ToroidalNum(y, architectureRules.min.y, architectureRules.max.y);

					var cellKey = x + '~' + y;

					if (!cells.has(cellKey)) {
						(function () {

							var thisCell = new CellArchitecture.VonNeumanToroidal({
								x: x,
								y: y,
								state: States[0]
							});
							thisCell.initGeom({
								cellGeometryClass: cellGeometryClass,
								architectureRules: architectureRules,
								scene: scene
							});

							cells.set(cellKey, thisCell);

							// Create the neighbours in a queue so that the stack won't overflow
							Utils.NextTick(function () {

								thisCell.neighbourKeys = {
									up: CellArchitecture.VonNeumanToroidal.Generate(cellGeometryClass, cells, scene, architectureRules, { x: x, y: y + 1 }),
									right: CellArchitecture.VonNeumanToroidal.Generate(cellGeometryClass, cells, scene, architectureRules, { x: x - 1, y: y }),
									down: CellArchitecture.VonNeumanToroidal.Generate(cellGeometryClass, cells, scene, architectureRules, { x: x, y: y - 1 }),
									left: CellArchitecture.VonNeumanToroidal.Generate(cellGeometryClass, cells, scene, architectureRules, { x: x + 1, y: y })
								};
							});
						})();
					}

					return cellKey;
				})();
			}
		}]);

		return VonNeumanToroidal;
	})(VonNeumanBasic);

	;

	return {
		VonNeumanBasic: VonNeumanBasic,
		VonNeumanToroidal: VonNeumanToroidal
	};
})();

var CellGeometry = {
	VonNeumanSquare: (function () {
		function VonNeumanSquare(_ref11) {
			var x = _ref11.x;
			var y = _ref11.y;
			var state = _ref11.state;
			var architecture = _ref11.architecture;
			var architectureRules = _ref11.architectureRules;
			var scene = _ref11.scene;

			_classCallCheck(this, VonNeumanSquare);

			this.mesh = new THREE.Mesh(new THREE.BoxGeometry(1, 1, 1), new THREE.MeshBasicMaterial({
				color: Colors[state]
			}));
			this.mesh.scale.y = state / 4 + 0.001; // If the cube's scale is ever zero it will cause 3js to crash

			this.mesh.architecture = architecture;

			this.mesh.position.x = x;
			this.mesh.position.y = 0;
			this.mesh.position.z = y;
			this.mesh.doubleSided = true;

			scene.add(this.mesh);
		}

		_createClass(VonNeumanSquare, [{
			key: 'update',
			value: function update(_ref12) {
				var state = _ref12.state;

				this.mesh.scale.y = state / 4 + 0.001; // If the cube's scale is ever zero it will cause 3js to crash
				this.mesh.material.color.setHex(Colors[state]);
			}
		}]);

		return VonNeumanSquare;
	})()
};
