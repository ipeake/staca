'use strict';

const States = {
	0: 0,
	1: 1,
	2: 2,
	3: 3,
	4: 4,
	5: 5,
	6: 6,
	7: 7,
	length: 8,
	previous: {
		0: 7,
		1: 0,
		2: 1,
		3: 2,
		4: 3,
		5: 4,
		6: 5,
		7: 6
	},
	next: {
		0: 1,
		1: 2,
		2: 3,
		3: 4,
		4: 5,
		5: 6,
		6: 7,
		7: 0
	}
};

const Colors = {
	0: 0xffffff,
	1: 0xff0000,
	2: 0x00ff00,
	3: 0x0000ff,
	4: 0xffff00,
	5: 0xff00ff,
	6: 0x00ffff,
	7: 0xff9999
};

class ACA extends Observable {
	constructor(
		{
			domElement,
			architecture,
			architectureRules,
			geometry,
			updateFrequencyMS = 100,
			updatesPerUpdate = 10,
			camera = {
				fov: 70,
				aspect: DOM.prop.width(domElement) / DOM.prop.height(domElement),
				near: 1,
				far: 100
			},
			cameraPosition = {x: 0, y: 0, z: 0}
		}
	) {
		super();

		this._threeScene = new THREE.Scene();
		this._threeCamera = new THREE.PerspectiveCamera(camera.fov, camera.aspect, camera.near, camera.far);
		this._threeCamera.position.set(cameraPosition.x, cameraPosition.y, cameraPosition.z);
		this._threeCamera.lookAt(this._threeScene.position);
		this._threeRenderer = new THREE.WebGLRenderer();
		this._threeRenderer.setSize(DOM.prop.width(domElement), DOM.prop.height(domElement));

		this._threeEffect = new THREE.AnaglyphEffect(this._threeRenderer);
		this._threeEffect.setSize(DOM.prop.width(domElement), DOM.prop.height(domElement));

		this._cells = new OMap();
		this._cells.addListener(
			'addedData',
			() => {

				this.redraw();
			}
		);

		this._rules = new OMap();
		this._rules.addListener(
			'*',
			data => {

				switch (data.type) {
					case 'addedData': return this._triggerEvent('addedRule', data);
					case 'removedData': return this._triggerEvent('removedRule', data);
				}
			}
		);

		this._simulationRunning = false;
		this._interval = null;
		this._updateFrequencyMS = updateFrequencyMS;
		this._updatesPerUpdate = updatesPerUpdate;
		this._cellArchitecture = architecture;
		this._cellArchitecture.Generate(geometry, this._cells, this._threeScene, architectureRules);

		DOM.modify.addListeners(
			this._threeRenderer.domElement,
			{
				click: event => {
					
					const cell = this.getCellAtPosition(event.clientX, event.clientY);

					if (cell) {

						cell.leftClick();

						this.redraw();
					}
				},
				contextmenu: event => {
					
					const cell = this.getCellAtPosition(event.clientX, event.clientY);

					if (cell) {

						cell.rightClick();

						this.redraw();
					}

					event.preventDefault();
				}
			}
		);

		DOM.modify.appendNode(
			domElement,
			this._threeRenderer.domElement
		);
	}

	isRunning() {

		return this._simulationRunning;
	}
	startRunning() {

		if (this._simulationRunning) return;

		this._simulationRunning = true;
		this._interval = setInterval(
			() => {

				for (let i = 0; i < this._updatesPerUpdate; ++i) this.updateCell();

				this.redraw();
			},
			this._updateFrequencyMS
		);
	}
	stopRunning() {

		if (!this._simulationRunning) return;

		this._simulationRunning = false;
		clearInterval(this._interval);
	}

	updateCell() {

		this.getRandomCell().update(this._cells, this._rules);
	}
	setAllCells(state) {

		this._cells.forEach(
			({value}) => value.set(state)
		);

		this.redraw();
	}
	getRandomCell() {

		return this.getCell(
			Math.floor(
				this._cells.size() * Math.random()
			)
		);
	}
	getCell(i) {

		return this._cells.entry(i);
	}
	getCellKey(k) {

		return this._cells.get(k);
	}
	getCellAtPosition(x, y) {

		const {left, top, width, height} = this._threeRenderer.domElement.getBoundingClientRect();

		// Make a vector from -1 to 1 where -1 is left/top and 1 is right/bottom
		const vector = new THREE.Vector3(
			2 * (x - left) / width - 1,
			-2 * (y - top) / height + 1,
			0.5
		);

		vector.unproject(this._threeCamera);

		const intersected = new THREE.Raycaster(
			this._threeCamera.position,
			vector.sub(this._threeCamera.position).normalize()
		).intersectObjects(
			this._threeScene.children
		);

		if (intersected.length) {

			const first = intersected[0];

			if (first.object && first.object.architecture) {

				return first.object.architecture;
			}
		}
	}

	addRule(acaRule) {

		this._cellArchitecture.AddRule(this._rules, acaRule);
	}
	removeRule(acaRule) {

		this._cellArchitecture.RemoveRule(this._rules, acaRule);
	}
	getRulesCompressed() {

		return this._rules.toJSONString();
	}
	loadRulesCompressed(jsonstring) {

		this.clearRules();

		this._rules.fromJSONString(jsonstring);
	}
	clearRules() {

		this._rules.empty();
	}

	adjustCameraPosition({x, y, z}) {

		this.setCameraPosition(
			{
				x: this._threeCamera.position.x + x,
				y: this._threeCamera.position.y + y,
				z: this._threeCamera.position.z + z
			}
		);
	}
	setCameraPosition({x, y, z}) {

		this._threeCamera.position.set(x, y, z);
		this._threeCamera.lookAt(this._threeScene.position);

		this.redraw();
	}
	redraw() {

		const ms = 50;
		const timeout = () => {

			if (!this.drawAgain) {
			
				this.drawnRecently = false;

				return;
			}

			this.forceRedraw();

			setTimeout(timeout, ms);
		};

		if (this.drawnRecently) {

			this.drawAgain = true;
		} else {

			this.forceRedraw();

			this.drawnRecently = true;

			setTimeout(timeout, ms);
		}
	}
	forceRedraw() {

		//this._threeRenderer.render(this._threeScene, this._threeCamera);
		this._threeEffect.render(this._threeScene, this._threeCamera);
	}
	dumpToImage() {

		// Need to redraw so frame buffer is full
		this.redraw();

		return DOM.gen.element(
			{
				type: 'img',
				styles: {
					width: DOM.prop.width(this._threeRenderer.domElement),
					height: DOM.prop.height(this._threeRenderer.domElement)
				},
				attributes: {
					src: this._threeRenderer.domElement.toDataURL()
				}
			}
		);
	}
};

const CellArchitecture = (
	() => {

		class GenericArchitecture {

			constructor({state}) {

				this.state = state;
			}

			update(cells, rules) {

				const state = rules.get(
					this.getStateAsString(cells)
				);

				if (T.Undefined(state) || state == this.state) return;

				this.set(state);
			}

			set(state) {

				this.state = state;

				this.cellGeometry.update({state});
			}

			leftClick() {

				this.state = States.next[this.state];
				this.cellGeometry.update({state: this.state});
			}

			rightClick() {

				this.state = States.previous[this.state];
				this.cellGeometry.update({state: this.state});
			}
		};

		class VonNeumanBasic extends GenericArchitecture {

			constructor(params) {

				super(params);

				const {x, y} = params;

				this.neighbourKeys = {
					up: void 0,
					right: void 0,
					down: void 0,
					left: void 0
				};
				this.x = x;
				this.y = y;
			}

			initGeom(
				{cellGeometryClass, architectureRules, scene}
			) {

				this.cellGeometry = new cellGeometryClass(
					{
						x: this.x,
						y: this.y,
						state: this.state,
						architectureRules,
						architecture: this,
						scene
					}
				);

				this.cellGeometry.update({state: this.state});
			}

			getStateAsString(cells) {
			
				return this.state + '~' + 
					cells.get(this.neighbourKeys.up).state + '~' +
					cells.get(this.neighbourKeys.right).state + '~' +
					cells.get(this.neighbourKeys.down).state + '~' +
					cells.get(this.neighbourKeys.left).state;
			}

			static AddRule(
				rules,
				{center, up, right, down, left, result, symmetric = true, reflective = true}
			) {

				const add = (center, up, right, down, left) => {

					rules.set(center + '~' + up + '~' + right + '~' + down + '~' + left, result);

					if (reflective) {

						rules.set(center + '~' + up + '~' + left + '~' + down + '~' + right, result);
					}
				}

				add(center, up, right, down, left);

				if (symmetric) {

					add(center, left, up, right, down);
					add(center, down, left, up, right);
					add(center, right, down, left, up);
				}
			}

			static RemoveRule(rules, {center, up, right, down, left, result}) {

				rules.delete(center + '~' + up + '~' + right + '~' + down + '~' + left);
			}

			static Generate(
				cellGeometryClass,
				cells,
				scene,
				architectureRules,
				{x, y} = architectureRules.firstCells,
				remainingIterations = architectureRules.iterations
			) {

				const cellKey = `${x}~${y}`;

				if (!cells.has(cellKey)) {

					let thisCell = new CellArchitecture.VonNeumanBasic(
						{
							x,
							y,
							state: States[0]
						}
					);
					thisCell.initGeom(
						{
							cellGeometryClass,
							architectureRules,
							scene
						}
					);

					cells.set(cellKey, thisCell);

					if (remainingIterations - 1 > 0) {

						// Create the neighbours in a queue so that the stack won't overflow
						Utils.NextTick(
							() => {

								thisCell.neighbourKeys = {
									up: CellArchitecture.VonNeumanBasic.Generate(
										cellGeometryClass, 
										cells,
										scene,
										architectureRules,
										{x: x, y: y + 1},
										remainingIterations - 1
									),
									right: CellArchitecture.VonNeumanBasic.Generate(
										cellGeometryClass, 
										cells,
										scene,
										architectureRules,
										{x: x - 1, y: y},
										remainingIterations - 1
									),
									down: CellArchitecture.VonNeumanBasic.Generate(
										cellGeometryClass, 
										cells,
										scene,
										architectureRules,
										{x: x, y: y - 1},
										remainingIterations - 1
									),
									left: CellArchitecture.VonNeumanBasic.Generate(
										cellGeometryClass, 
										cells,
										scene,
										architectureRules,
										{x: x + 1, y: y},
										remainingIterations - 1
									)
								};
							}
						);
					}
				}

				return cellKey;
			}
		};

		class VonNeumanToroidal extends VonNeumanBasic {

			constructor(params) {

				super(params);
			}

			static Generate(
				cellGeometryClass,
				cells,
				scene,
				architectureRules,
				{x, y} = architectureRules.firstCells
			) {

				x = Utils.ToroidalNum(x, architectureRules.min.x, architectureRules.max.x);
				y = Utils.ToroidalNum(y, architectureRules.min.y, architectureRules.max.y);

				const cellKey = `${x}~${y}`;

				if (!cells.has(cellKey)) {

					let thisCell = new CellArchitecture.VonNeumanToroidal(
						{
							x,
							y,
							state: States[0]
						}
					);
					thisCell.initGeom(
						{
							cellGeometryClass,
							architectureRules,
							scene
						}
					);

					cells.set(cellKey, thisCell);

					// Create the neighbours in a queue so that the stack won't overflow
					Utils.NextTick(
						() => {

							thisCell.neighbourKeys = {
								up: CellArchitecture.VonNeumanToroidal.Generate(
									cellGeometryClass, 
									cells,
									scene,
									architectureRules,
									{x: x, y: y + 1}
								),
								right: CellArchitecture.VonNeumanToroidal.Generate(
									cellGeometryClass, 
									cells,
									scene,
									architectureRules,
									{x: x - 1, y: y}
								),
								down: CellArchitecture.VonNeumanToroidal.Generate(
									cellGeometryClass, 
									cells,
									scene,
									architectureRules,
									{x: x, y: y - 1}
								),
								left: CellArchitecture.VonNeumanToroidal.Generate(
									cellGeometryClass, 
									cells,
									scene,
									architectureRules,
									{x: x + 1, y: y}
								)
							};
						}
					);
				}

				return cellKey;
			}
		};

		return {
			VonNeumanBasic,
			VonNeumanToroidal
		};
	}
)();

const CellGeometry = {
	VonNeumanSquare: class VonNeumanSquare {
		constructor(
			{x, y, state, architecture, architectureRules, scene}
		) {

			this.mesh = new THREE.Mesh(
				new THREE.BoxGeometry(1, 1, 1),
				new THREE.MeshBasicMaterial(
					{
						color: Colors[state]
					}
				)
			);
			this.mesh.scale.y = state / 4 + 0.001; // If the cube's scale is ever zero it will cause 3js to crash

			this.mesh.architecture = architecture;

			this.mesh.position.x = x;
			this.mesh.position.y = 0;
			this.mesh.position.z = y;
			this.mesh.doubleSided = true;

			scene.add(this.mesh);
		}

		update(
			{state}
		) {

			this.mesh.scale.y = state / 4 + 0.001; // If the cube's scale is ever zero it will cause 3js to crash
			this.mesh.material.color.setHex(Colors[state]);
		}
	}
};
