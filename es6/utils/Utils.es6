'use strict';

const Utils = {
	NextTick: func => setTimeout(func, 0),
	ToroidalNum: (num, min, max) => num == min - 1 ? max : num == max + 1 ? min : num,
	SaveFile: (
		() => {

			let a = document.createElement('a');

			return ({title, text, type = 'text/plain'}) => {

				a.href = URL.createObjectURL(new Blob([text], {type}));
				a.download = title;

				a.click();
			}
		}
	)(),
	LoadFile: (
		() => {

			let i = DOM.gen.element(
				{
					type: 'input',
					styles: {
						display: 'none'
					},
					attributes: {
						type: 'file'
					}
				}
			);
			
			DOM.modify.addToBody(i);

			return callback => {

				i.onchange = event => {

					Array.prototype.forEach.call(
						event.target.files,
						file => {

							let reader = new FileReader();
							reader.onload = e => callback(e.target.result);
							reader.readAsText(file);
						}
					);
				};
				i.click();
			}
		}
	)()
};