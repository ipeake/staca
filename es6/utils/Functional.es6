"use strict";

const Functional = {
	curry: (
		() => {
			
			const _ = Symbol();

			let curry = (func, ...args) => {

				const innerFunc = (...innerArgs) => {

					let innerI = 0;
					let theseArgs = args.slice();

					for (let i = 0; i < theseArgs.length && innerI < innerArgs.length; ++i) {

						if (theseArgs[i] === _) {

							theseArgs[i] = innerArgs[innerI];

							++innerI;
						}
					}

					theseArgs = theseArgs.concat(
						innerArgs.slice(innerI)
					);

					if (
						theseArgs.length >= func.length &&
						theseArgs.every(x => x !== _)
					) {

						return func(...theseArgs);
					} else {

						args = theseArgs;

						return innerFunc;
					}
				};

				return innerFunc;
			}

			curry._ = _;

			return curry;
		}
	)(),
	// Properties
	first: (xs) => xs[0],
	nth: (xs, n) => n >= 0 ? xs[n] : xs[length + n],
	last: (xs) => xs[xs.length - 1],
	keys: (obj) => Object.keys(obj),
	keyValuePairs: (obj) => Functional.map(Functional.keys(obj), (key) => ({key, value: obj[key]})),
	reverse: (arrayLike) => Functional.cloneShallow(arrayLike).reverse(),
	// Iteration
	each: (arrayLike, func) => {

		Array.prototype.forEach.call(arrayLike, func);

		return arrayLike;
	},
	eachKey: (obj, func) => Functional.each(Functional.keyValuePairs(obj), func),
	any: (arrayLike, predicate) => Array.prototype.some.call(arrayLike, predicate),
	anyWithRes: (arrayLike, predicate) => {

		let res;

		Functional.any(
			arrayLike,
			x => res = predicate(x)
		);

		return res || void 0;
	},
	all: (arrayLike, predicate) => Array.prototype.every.call(arrayLike, predicate),
	fold: (arrayLike, func, initialVal) => Array.prototype.reduce.call(arrayLike, func, initialVal),
	map: (arrayLike, func) => Array.prototype.map.call(arrayLike, func),
	cloneShallow: (arrayLike) => Array.prototype.slice.call(arrayLike)
};