'use strict';

const DOM = {
	el: {
		class: (par, clas) => par.getElementsByClassName(clas),
		id: (id) => document.getElementById(id),
		type: (par, tagname) => par.getElementsByTagName(tagname),
		query: (par, query) => par.querySelectorAll(query),
		queryFirst: (par, query) => par.querySelector(query)
	},
	prop: {
		nodes: (el) => el.childNodes,
		firstNode: (el) => el.firstChild,
		nthNode: (el, i) => el.childNodes[i],
		lastNode: (el) => el.lastChild,
		elements: (el) => el.children,
		firstElement: (el) => el.firstElementChild,
		nthElement: (el, i) => el.children[i],
		lastElement: (el) => el.lastElementChild,
		width: (el) => el.clientWidth,
		height: (el) => el.clientHeight,
		left: (el) => el.offsetLeft,
		top: (el) => el.offsetTop
	},
	modify: {
		prependNode: (el, child) => {

			el.insertBefore(
				child,
				DOM.prop.firstNode(el)
			);

			return el;
		},
		insertNode: (el, i) => {

			el.insertBefore(
				child,
				DOM.prop.nthNode(el, i)
			);

			return el;
		},
		appendNode: (el, child) => {

			el.appendChild(child)

			return el;
		},
		prependNodes: (el, children) => {

			Functional.each(
				Functional.reverse(children),
				Functional.curry(DOM.modify.prependNode, el)
			);
		},
		insertNodes: (el, children, i) => {

			Functional.each(
				Functional.reverse(children),
				Functional.curry(DOM.modify.insertNode, Functional._, i)
			)

			return el;
		},
		appendNodes: (el, children) => {

			Functional.each(
				children,
				Functional.curry(DOM.modify.appendNode, el)
			);

			return el;
		},
		addClass: (el, clas) => {

			el.classList.add(clas);

			return el;
		},
		addClasses: (el, classes) => {

			Functional.each(
				classes,
				Functional.curry(DOM.modify.addClass(el))
			);

			return el;
		},
		addStyles: (el, styles) => {

			Functional.eachKey(styles, ({key, value}) => el.style[key] = value);

			return el;
		},
		addListeners: (el, listeners) => {

			Functional.eachKey(listeners, ({key, value}) => el.addEventListener(key, value));

			return el;
		},
		addToBody: (
			() => {

				let queue = [];

				document.addEventListener(
					'DOMContentLoaded',
					() => {

						DOM.modify.appendNodes(document.body, queue);

						queue = null;
						DOM.modify.addToBody = Functional.curry(DOM.modify.appendNode, document.body);
					}
				);

				return (node) => {

					queue.push(node);

					return node;
				}
			}
		)
	},
	gen: {
		element: (
			{
				type,
				id,
				classes,
				nodes,
				styles,
				listeners,
				attributes,
				data
			}
		) => {

			let newE = document.createElement(type);

			if (classes) DOM.modify.addClasses(newE, classes);
			if (id) newE.id = id;
			if (styles) DOM.modify.addStyles(newE, styles);
			if (listeners) DOM.modify.addListeners(newE, listeners);
			if (nodes) DOM.modify.appendNodes(newE, nodes);
			if (attributes) Functional.eachKey(attributes, ({key, value}) => newE[key] = value);
			if (data) Functional.eachKey(data, ({key, value}) => newE.dataset[key] = value);

			return newE;
		},
		text: (content) => document.createTextNode(content)
	}
};