'use strict';

const T = {
	Null: (x) => x === null && T.Null,
	Undefined: (x) => x === undefined && T.Undefined,
	Number: (x) => x.__proto__ === Number && T.Number,
	Integer: (x) => T.Number(x) && x === (x | 0) && T.Integer,
	Float: (x) => T.Number(x) && !T.Integer(x) && T.Float,
	String: (x) => x.__proto__ === String && T.String,
	is: (type, x) => T.Undefined(x) || type(x) ||  T.is(T.__proto__, x),
	first: (types, x) => T.Undefined(x) || Functional.anyWithRes(types, type) || T.first(types, x.__proto__)
};