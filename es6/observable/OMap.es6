'use strict';

class OMap extends Observable {

	constructor() {
		super();

		this._o = {};
	}

	get(key) {

		return this._o[key];
	}

	set(key, value) {

		this.delete(key);

		this._o[key] = value;

		this._triggerEvent('addedData', {key, value});
	}

	has(key) {

		return !T.Undefined(this.get(key));
	}

	size() {

		return this.keys().length;
	}

	delete(key) {

		if (!T.Undefined(this._o[key])) {

			const value = this._o[key];

			delete this._o[key];

			this._triggerEvent('removedData', {key, value});
		}
	}

	forEach(f) {

		Functional.eachKey(this._o, f);
	}

	key(i) {

		return Functional.keys(this._o)[i];
	}

	keys() {
	
		return Functional.keys(this._o);	
	}

	entry(i) {

		return this._o[this.key(i)];
	}

	entries() {

		return Functional.keyValuePairs(this._o);
	}

	empty() {

		this.forEach(
			({key}) => this.delete(key)
		);
	}

	toJSONString() { return JSON.stringify(this._o); }
	fromJSONString(jsons) {

		this.empty();

		this._o = JSON.parse(jsons);
		this.forEach(
			({key, value}) => this._triggerEvent('addedData', {key, value})
		);
	}
};