'use strict';

class Observable {

	constructor() {

		this._listeners = {};
	}

	_triggerEvent(type, data) {

		if (data) {

			data.type = type;
		} else {

			data = {type};
		}

		if (this._listeners[type]) {

			this._listeners[type].forEach(l => l(data));
		}

		if (type !== '*' && this._listeners['*']) {
		
			this._listeners['*'].forEach(l => l(data));
		}
	}
	addListener(type, listener) {

		(this._listeners[type] || (this._listeners[type] = new Set())).add(listener);
	}
	removeListener(type, listener) {

		if (this._listeners[type]) this._listeners[type].delete(listener);
	}
};