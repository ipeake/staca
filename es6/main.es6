'use strict';

const simulationACA = new ACA(
	{
		domElement: DOM.el.id('simulation'),
		architecture: CellArchitecture.VonNeumanToroidal,
		architectureRules: {
			min: {x: -25, y: -25},
			firstCells: {x: 10, y: 10},
			max: {x: 25, y: 25}
		},
		geometry: CellGeometry.VonNeumanSquare,
		updateFrequencyMS: 10,
		updatesPerUpdate: 20,
		cameraPosition: {x: 30, y: 50, z: 30}
	}
);

let dragPos = {};
let cameraRotation = {horizontal: 0, vertical: 0};

DOM.modify.addListeners(
	DOM.el.id('simulation'),
	{
		dragstart: (event) => {

			dragPos.x = event.clientX;
			dragPos.y = event.clientY;
		},
		drag: (event) => {

			if (!event.clientX && !event.clientY) return;

			const dragChange = {x: event.clientX - dragPos.x, y: event.clientY - dragPos.y};

			dragPos.x = event.clientX;
			dragPos.y = event.clientY;

			cameraRotation.horizontal += dragChange.x / 150;
			cameraRotation.vertical -= dragChange.y / 150;

			if (cameraRotation.vertical < .1) cameraRotation.vertical = .1;
			else if (cameraRotation.vertical > Math.PI / 2 - .1) cameraRotation.vertical = Math.PI / 2 - .1;

			simulationACA.setCameraPosition(
				{
					x: 30 * Math.sin(cameraRotation.horizontal) * Math.cos(cameraRotation.vertical),
					y: 30 * Math.sin(cameraRotation.vertical),
					z: 30 * Math.cos(cameraRotation.horizontal) * Math.cos(cameraRotation.vertical)
				}
			);
		}
	}
);

let playpause = DOM.el.id('simulation-controls-playpause');

DOM.modify.addListeners(
	playpause,
	{
		click: () => {

			if (simulationACA.isRunning()) {

				simulationACA.stopRunning();
				playpause.textContent = 'Run simulation';	
			} else {

				simulationACA.startRunning();
				playpause.textContent = 'Pause simulation';
			}
		}
	}
);

DOM.modify.addListeners(
	DOM.el.id('simulation-controls-step'),
	{
		click: () => {

			simulationACA.stopRunning();
			playpause.textContent = 'Run simulation';	

			simulationACA.updateCell();
			simulationACA.redraw();
		}
	}
);

DOM.modify.addListeners(
	DOM.el.id('simulation-controls-reset'),
	{
		click: () => simulationACA.setAllCells(0)
	}
);

const rulesACA = new ACA(
	{
		domElement: DOM.el.id('rules'),
		architecture: CellArchitecture.VonNeumanBasic,
		architectureRules: {
			firstCells: {x: 0, y: 0},
			iterations: 2
		},
		geometry: CellGeometry.VonNeumanSquare,
		updateFrequencyMS: Infinity,
		cameraPosition: {x: 0, y: 8, z: 0}
	}
);

const resultACA = new ACA(
	{
		domElement: DOM.el.id('result'),
		architecture: CellArchitecture.VonNeumanBasic,
		architectureRules: {
			firstCells: {x: 0, y: 0},
			iterations: 1
		},
		geometry: CellGeometry.VonNeumanSquare,
		updateFrequencyMS: Infinity,
		cameraPosition: {x: 0, y: 8, z: 0}
	}
);

const symmetrical = DOM.el.id('rule-controls-symmetrical');
const reflective = DOM.el.id('rule-controls-reflective');

let ruleDisplay = DOM.el.id('rule-display');

DOM.modify.addListeners(
	DOM.el.id('rule-controls-saveconfig'),
	{
		click: (event) => {

			const center = rulesACA.getCell(0);
			const result = resultACA.getCell(0);

			simulationACA.addRule(
				{
					center: center.state,
					up: rulesACA.getCellKey(center.neighbourKeys.up).state,
					right: rulesACA.getCellKey(center.neighbourKeys.right).state,
					down: rulesACA.getCellKey(center.neighbourKeys.down).state,
					left: rulesACA.getCellKey(center.neighbourKeys.left).state,
					result: result.state,
					symmetric: symmetrical.checked,
					reflective: reflective.checked
				}
			);
		}
	}
);

simulationACA.addListener(
	'addedRule',
	({key, value}) => {
		
		const keys = key.split('~');

		const center = +keys[0];
		const up = +keys[1];
		const right = +keys[2];
		const down = +keys[3];
		const left = +keys[4];
		const result = value;

		const centerCell = rulesACA.getCell(0);
		const upCell = rulesACA.getCellKey(centerCell.neighbourKeys.up);
		const rightCell = rulesACA.getCellKey(centerCell.neighbourKeys.right);
		const downCell = rulesACA.getCellKey(centerCell.neighbourKeys.down);
		const leftCell = rulesACA.getCellKey(centerCell.neighbourKeys.left);
		const resultCell = resultACA.getCell(0);

		const centerState = centerCell.state;
		const upState = upCell.state;
		const rightState = rightCell.state;
		const downState = downCell.state;
		const leftState = leftCell.state;
		const resultState = resultCell.state;

		centerCell.set(center);
		upCell.set(up);
		rightCell.set(right);
		downCell.set(down);
		leftCell.set(left);
		resultCell.set(result);

		rulesACA.forceRedraw();
		resultACA.forceRedraw();

		DOM.modify.appendNode(
			ruleDisplay,
			DOM.gen.element(
				{
					type: 'div',
					styles: {
						display: 'inline-block'
					},
					nodes: [
						rulesACA.dumpToImage(),
						resultACA.dumpToImage()
					],
					data: {
						key
					},
					listeners: {
						click: () => {

							simulationACA.removeRule({center,up, right, down, left});
						}
					}
				}
			)
		);

		centerCell.set(centerState);
		upCell.set(upState);
		rightCell.set(rightState);
		downCell.set(downState);
		leftCell.set(leftState);
		resultCell.set(resultState);

		rulesACA.forceRedraw();
		resultACA.forceRedraw();
	}
);

simulationACA.addListener(
	'removedRule',
	({key}) => {
		
		DOM.el.queryFirst(
			ruleDisplay,
			`div[data-key="${key}"]`
		).remove();
	}
);

DOM.modify.addListeners(
	DOM.el.id('loadsave-controls-saverules'),
	{
		click: () => {

			Utils.SaveFile(
				{
					title: Date.now() + '.acarules',
					text: simulationACA.getRulesCompressed()
				}
			);
		}
	}
);

DOM.modify.addListeners(
	DOM.el.id('loadsave-controls-loadrules'),
	{
		click: () => Utils.LoadFile(contents => simulationACA.loadRulesCompressed(contents))
	}
);

DOM.modify.addListeners(
	DOM.el.id('loadsave-controls-clearrules'),
	{
		click: () => simulationACA.clearRules()
	}
);