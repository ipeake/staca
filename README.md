# README #

### What is this repository for? ###

* The aim of this project is to visualize and simulate a asynchronous cellular automata using JS.
* 0.1

### How do I get set up? ###

* Summary of set up
* Simply download the files and open index.html in a browser
* Compilation
* The JS source files should be compiled using babebljs. This can be done online at https://babeljs.io/repl/. Please keep a copy of the es6 source as $filename.es6.js and a copy of the compiled code as $filename.js.

### Contribution guidelines ###

* Guidelines
* The JS source should follow the strict rules defined in https://github.com/JulianAustralia/javascript. This has not been update for es6 yet, so please raise any issues with JulianAustralia.

### Who do I talk to? ###

* Contact
* Julian Watson at https://bitbucket.org/JulianAustralia/